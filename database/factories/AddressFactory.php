<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Address;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'street' => $faker->street,
        'housenumber' => $faker->buildingNumber,
        'zip' => $faker->postcode,
        'city'=> $faker->city,
        'country'=> $faker->country,
    ];
});
