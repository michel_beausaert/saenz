<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{

    /**
     * Get all of the member addresses.
     */
    public function addresses()
    {
        return $this->morphMany('Address::class', 'addressable');
    }
}
