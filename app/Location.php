<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    /**
     * Get all of the location addresses.
     */
    public function addresses()
    {
        return $this->morphMany('Address::class', 'addressable');
    }
}
