<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressType extends Model
{
    /*
     * get all addresses of this type
     */
    public function addresses()
    {
        return $this->hasMany('App\Address', 'address_type_id');
    }
}
