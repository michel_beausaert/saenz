<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

    /**
     * Get all of the owning addressable models.
     */
    public function addressable()
    {
        return $this->morphTo();
    }

    /*
     * get the type of his adddress
     */
    public function type() {
        return $this->belongsTo('App\AddressType', 'address_type_id');
    }
}
