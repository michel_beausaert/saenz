<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SAENZ </title>

    <!-- Bootstrap -->
    <link href="{{ asset('assets/dashboard/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('assets/dashboard/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('assets/dashboard/vendors/nprogress/nprogress.css') }}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('assets/dashboard/vendors/bootstrap-daterangepicker/daterangepicker.css') }} " rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset('assets/dashboard/build/css/custom.min.css') }}" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
    @include('shared/dashboard/nav-sidebar')
    @include('shared/dashboard/nav')
    @yield('content');'
    @include('shared/dashboard/footer')
    </div>
</div>
@section('javascript')
<!-- jQuery -->
<script src="{{ asset('assets/dashboard/vendors/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('assets/dashboard/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('assets/dashboard/vendors/fastclick/lib/fastclick.js') }}"></script>
<!-- NProgress -->
<script src="{{ asset('assets/dashboard/vendors/nprogress/nprogress.js') }}"></script>
<!-- Chart.js -->
<script src="{{ asset('assets/dashboard/vendors/Chart.js/dist/Chart.min.js') }}"></script>
<!-- jQuery Sparklines -->
<script src="{{ asset('assets/dashboard/vendors/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- Flot -->
<script src="{{ asset('assets/dashboard/vendors/Flot/jquery.flot.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/Flot/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/Flot/jquery.flot.time.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/Flot/jquery.flot.stack.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/Flot/jquery.flot.resize.js') }}"></script>
<!-- Flot plugins -->
<script src="{{ asset('assets/dashboard/vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/flot.curvedlines/curvedLines.js') }}"></script>
<!-- DateJS -->
<script src="{{ asset('assets/dashboard/vendors/DateJS/build/date.js') }}"></script>
<!-- bootstrap-daterangepicker -->
<script src="{{ asset('assets/dashboard/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<!-- Custom Theme Scripts -->
<script src="{{ asset('assets/dashboard/build/js/custom.min.js') }}"></script>
@show
</body>
</html>